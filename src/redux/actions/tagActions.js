import * as types from "./actionTypes";
import * as TagsApi from "../../api/tagsApi";
import { beginApiCall, apiCallError } from "./apiStatusActions";

export function loadTagsSuccess(tags) {
  return { type: types.LOAD_TAGS_SUCCESS, tags };
}

export function loadTags() {
  return function (dispatch) {
    dispatch(beginApiCall());
    return TagsApi.getTags()
      .then((data) => {
        dispatch(loadTagsSuccess(data.tags));
      })
      .catch((error) => {
        dispatch(apiCallError());
        throw error;
      });
  };
}
