export const CHANGE_ORDER = "CHANGE_ORDER";
export const FETCH_TEXTS = "FETCH_TEXTS";
export const FILLED_LIST = "FILLED_LIST";
export const FETCH_TEXTS_API = "FETCH_TEXTS_API";

export const LOAD_TAGS_SUCCESS = "LOAD_TAGS_SUCCESS";
export const LOAD_CARDS_SUCCESS = "LOAD_CARDS_SUCCESS";
export const CREATE_CARD_SUCCESS = "CREATE_CARD_SUCCESS";
export const UPDATE_CARD_SUCCESS = "UPDATE_CARD_SUCCESS";
export const BEGIN_API_CALL = "BEGIN_API_CALL";
export const API_CALL_ERROR = "API_CALL_ERROR";

// By convention, actions that end in "_SUCCESS" are assumed to have been the result of a completed
// API call. But since we're doing an optimistic delete, we're hiding loading state.
// So this action name deliberately omits the "_SUCCESS" suffix.
// If it had one, our apiCallsInProgress counter would be decremented below zero
// because we're not incrementing the number of apiCallInProgress when the delete request begins.
export const DELETE_CARD_OPTIMISTIC = "DELETE_CARD_OPTIMISTIC";

export const LOAD_CHAPTERS_SUCCESS = "LOAD_CHAPTERS_SUCCESS";
export const CREATE_CHAPTER_OPTIMISTIC = "CREATE_CHAPTER_OPTIMISTIC";
export const UPDATE_CHAPTER_OPTIMISTIC = "UPDATE_CHAPTER_OPTIMISTIC";
export const DELETE_CHAPTER_OPTIMISTIC = "DELETE_CHAPTER_OPTIMISTIC";

export const FILTERED_BY_TAGS = "FILTERED_BY_TAGS";
export const UPDATE_CHAPTER = "UPDATE_CHAPTER";
