import * as types from "./actionTypes";
import * as cardsApi from "../../api/cardsApi";
import { beginApiCall, apiCallError } from "./apiStatusActions";
import { saveChapter } from "./chapterActions";

export function createCardSuccess(card) {
  return { type: types.CREATE_CARD_SUCCESS, card };
}
export function updateCardSuccess(card) {
  return { type: types.UPDATE_CARD_SUCCESS, card };
}

export function loadCardSuccess(cards) {
  return { type: types.LOAD_CARDS_SUCCESS, cards };
}

export function deleteCardOptimistic(cardId) {
  return { type: types.DELETE_CARD_OPTIMISTIC, cardId };
}

export function filteredByTags(filterArr, arrTags) {
  return { type: types.FILTERED_BY_TAGS, filterArr, arrTags };
}

export function loadCards() {
  return function (dispatch) {
    dispatch(beginApiCall());
    return cardsApi
      .getCards()
      .then((data) => {
        dispatch(loadCardSuccess(data.cards));
      })
      .catch((error) => {
        dispatch(apiCallError());
        throw error;
      });
  };
}

export function saveCard(card) {
  return function (dispatch, getState) {
    let state = getState();
    dispatch(beginApiCall());
    return cardsApi
      .saveCard(card)
      .then((savedCard) => {
        if (card.id) {
          dispatch(updateCardSuccess(savedCard));
        } else {
          dispatch(createCardSuccess(savedCard));
          dispatch(
            saveChapter({
              ...state.chapters.draft,
              cardsRef: [...state.chapters.draft.cardsRef, savedCard.id],
            })
          );
        }
      })
      .catch((error) => {
        dispatch(apiCallError());
        throw error;
      });
  };
}
export function deleteCard(card) {
  return function (dispatch, getState) {
    // Doing optimistic delete, so not dispatching begin/end api call
    // actions, or apiCallError action since we're not showing the loading status for this.
    dispatch(deleteCardOptimistic(card.id));
    return cardsApi.deleteCard(card.id);
  };
}
