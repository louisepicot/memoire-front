import * as types from "./actionTypes";
import * as chaptersApi from "../../api/chaptersApi";
import { beginApiCall, apiCallError } from "./apiStatusActions";

export function createChapterOptimistic(chapter) {
  return { type: types.CREATE_CHAPTER_OPTIMISTIC, chapter };
}

export function updateChapterOptimistic(chapter) {
  return { type: types.UPDATE_CHAPTER_OPTIMISTIC, chapter };
}
export function loadChapterSuccess(chapters) {
  return { type: types.LOAD_CHAPTERS_SUCCESS, chapters };
}

export function deleteChapterOptimistic(chapterId) {
  return { type: types.DELETE_CHAPTER_OPTIMISTIC, chapterId };
}

export function loadChapters() {
  return function (dispatch) {
    dispatch(beginApiCall());
    return chaptersApi
      .getChapters()
      .then((data) => {
        dispatch(loadChapterSuccess(data.chapters));
      })
      .catch((error) => {
        dispatch(apiCallError());
        throw error;
      });
  };
}

export function saveChapter(chapter) {
  return function (dispatch) {
    // dispatch(beginApiCall());
    return chaptersApi
      .saveChapter(chapter)
      .then((savedChapter) => {
        chapter.id
          ? dispatch(updateChapterOptimistic(savedChapter))
          : dispatch(createChapterOptimistic(savedChapter));
      })
      .catch((error) => {
        dispatch(apiCallError());
        throw error;
      });
  };
}

export function deleteChapter(chapter) {
  return function (dispatch, getState) {
    // Doing optimistic delete, so not dispatching begin/end api call
    // actions, or apiCallError action since we're not showing the loading status for this.
    dispatch(deleteChapterOptimistic(chapter.id));
    return chaptersApi.deleteChapter(chapter.id);
  };
}

export const changeOrder = (newOrder) => {
  return {
    type: types.CHANGE_ORDER,
    newOrder,
  };
};
