import * as types from "../actions/actionTypes";
import initialState from "../data.js";

const filter = (state = initialState, action) => {
  switch (action.type) {
    case types.FILTERED_BY_TAGS:
      return action.filterArr;
    default:
      return state;
  }
};
export default filter;
