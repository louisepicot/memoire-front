import * as types from "../actions/actionTypes";
import initialState from "../data.js";

const chapters = (state = initialState.chapters, action) => {
  switch (action.type) {
    case types.LOAD_CHAPTERS_SUCCESS:
      return action.chapters;
    case types.CREATE_CHAPTER_OPTIMISTIC:
      let newState = { ...state };
      newState[action.chapter.id] = action.chapter;
      return newState;
    case types.UPDATE_CHAPTER_OPTIMISTIC:
      let updatedState = { ...state };
      updatedState[action.chapter.id] = action.chapter;
      return updatedState;
    case types.DELETE_CHAPTER_OPTIMISTIC:
      let newStateDelete = { ...state };
      delete newStateDelete[action.chapterId];
      return newStateDelete;
    case types.UPDATE_CHAPTER:
      let newStatedraft = {
        ...state,
        draft: {
          ...state.draft,
          cardsRef: [...state.draft.cardsRef, action.card.id],
        },
      };
      return newStatedraft;
    case types.CHANGE_ORDER:
      return action.newOrder;
    default:
      return state;
  }
};
export default chapters;
