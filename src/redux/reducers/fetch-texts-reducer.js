import * as types from "../actions/actionTypes";
import initialState from "../data.js";

export const cards = (state = initialState.cards, action) => {
  switch (action.type) {
    case types.FETCH_TEXTS:
      return [
        ...state,
        {
          id: state.length,
          padName: `${action.padName}`,
          html: action.payload,
        },
      ];
    case types.LOAD_CARDS_SUCCESS:
      return action.cards.cards;
    case types.CREATE_CARD:
      let keys = Object.keys(state).length;
      let copiedState = { ...state };
      copiedState[keys] = action.card;
      return copiedState;
    case types.CHANGE_ORDER:
      let result = {};
      action.newOrder.forEach((actionObj) => {
        Object.keys(state).forEach((key) => {
          if (`${actionObj.id}` === `${state[key].id}`) {
            result[Object.keys(result).length] = state[key];
          }
        });
      });
      return result;
    default:
      return state;
  }
};

export const navList = (state = initialState.navList, action) => {
  switch (action.type) {
    case types.FILLED_LIST:
      const newObject = {
        id: action.padId,
        h1: action.mainTitle,
        h2: action.subTilesArray,
      };
      return [...state, newObject];
    case types.CHANGE_ORDER: {
      return action.newOrder;
    }
    default: {
      return state;
    }
  }
};
