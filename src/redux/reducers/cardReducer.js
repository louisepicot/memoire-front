import * as types from "../actions/actionTypes";
import initialState from "../data.js";

const cards = (state = initialState.cards, action) => {
  switch (action.type) {
    case types.LOAD_CARDS_SUCCESS:
      return action.cards;
    case types.CREATE_CARD_SUCCESS:
      let newState = { ...state };
      newState[action.card.id] = action.card;
      return newState;
    case types.UPDATE_CARD_SUCCESS:
      let updatedState = { ...state };
      updatedState[action.card.id] = action.card;
      return updatedState;
    case types.DELETE_CARD_OPTIMISTIC:
      let newStateDelete = { ...state };
      delete newStateDelete[action.cardId];
      return newStateDelete;
    default:
      return state;
  }
};
export default cards;
