import { combineReducers } from "redux";
import apiCallsInProgress from "./apiStatusReducer";
import cards from "./cardReducer";
import tags from "./tagReducer";
import chapters from "./chapterReducer";
import filter from "./filterReducer";

const rootReducer = combineReducers({
  cards,
  tags,
  chapters,
  filter,
  apiCallsInProgress,
});

export default rootReducer;
