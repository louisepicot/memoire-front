import { handleResponse, handleError } from "./apiUtils";
//const baseUrl = "http://localhost:5000/chapters/";
const baseUrl = "https://back-outil-memoire.herokuapp.com/chapters/";
export function getChapters() {
  return fetch(baseUrl).then(handleResponse).catch(handleError);
}
export function saveChapter(chapter) {
  return fetch(baseUrl + (chapter.id || ""), {
    method: chapter.id ? "PUT" : "POST", // POST for create, PUT to update when id already exists.
    headers: { "content-type": "application/json" },
    body: JSON.stringify(chapter),
  })
    .then(handleResponse)
    .catch(handleError);
}

export function deleteChapter(chapterId) {
  return fetch(baseUrl + chapterId, { method: "DELETE" })
    .then(handleResponse)
    .catch(handleError);
}
