import { handleResponse, handleError } from "./apiUtils";
// const baseUrl = "http://localhost:5000/tags/";
const baseUrl = "https://back-outil-memoire.herokuapp.com/tags/";

export function getTags() {
  return fetch(baseUrl).then(handleResponse).catch(handleError);
}
