import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import ChapterBox from "../functional-components/components/ChapterBox";
import { DragDropContext, Droppable } from "react-beautiful-dnd";
import { saveChapter } from "../../api/chaptersApi";

const DraggableList = ({
  cards,
  changeOrder,
  chaptersOrder,
  chapters,
  onDelete,
}) => {
  const [items, setItems] = useState();
  const [order, setOrder] = useState([]);

  useEffect(() => {
    if (chaptersOrder.length > 0) {
      setOrder(chaptersOrder);
    }
  }, [chaptersOrder]);

  const onDragEnd = async (result) => {
    // changeOrder(items);
    const { destination, draggableId, source, type } = result;

    if (!destination) {
      return;
    }

    if (
      destination.droppableId === source.droppableId &&
      destination.index === source.index
    ) {
      return;
    }

    if (type === "chapter") {
      const newChaptersOrder = [...chaptersOrder];
      newChaptersOrder.splice(source.index, 1);
      newChaptersOrder.splice(destination.index, 0, draggableId);
      setOrder([...newChaptersOrder]);
      return;
    }

    const start = chapters[source.droppableId];
    const finish = chapters[destination.droppableId];

    if (start === finish) {
      const newCardsRef = [...start.cardsRef];
      newCardsRef.splice(source.index, 1);
      newCardsRef.splice(destination.index, 0, draggableId);

      const newChapter = { ...start, cardsRef: newCardsRef };
      changeOrder({
        ...chapters,
        [newChapter.id]: newChapter,
      });
    } else {
      const startNewCardsRef = [...start.cardsRef];
      startNewCardsRef.splice(source.index, 1);
      const newStartChapter = { ...start, cardsRef: startNewCardsRef };

      const finishNewCardsRef = [...finish.cardsRef];
      finishNewCardsRef.splice(destination.index, 0, draggableId);
      const newFinishChapter = { ...finish, cardsRef: finishNewCardsRef };

      await changeOrder({
        ...chapters,
        [newStartChapter.id]: newStartChapter,
        [newFinishChapter.id]: newFinishChapter,
      });
      await saveChapter(newStartChapter);
      await saveChapter(newFinishChapter);
    }
  };

  const onClick = (e, card) => {
    document.querySelectorAll(".card h1").forEach((h1) => {
      if (h1.innerText.toLowerCase() === card.title.toLowerCase()) {
        h1.parentNode.parentNode.scrollIntoView({
          block: "start",
          behavior: "smooth",
        });
      }
    });
  };

  return (
    <DragDropContext onDragEnd={onDragEnd}>
      <Droppable droppableId="all-columns" type="chapter">
        {(provided) => (
          <div
            id="reorder-list"
            {...provided.droppableProps}
            ref={provided.innerRef}
          >
            {Object.keys(chapters)[0] &&
              Object.keys(cards)[0] &&
              chaptersOrder.map((chapterId, index) => {
                // let arrCard = Object.keys(cards).map((key) => cards[key]);
                let column = chapters[chapterId];
                let cardsRef = chapters[chapterId].cardsRef.map(
                  (cardId) => cards[cardId]
                );
                // arrCard.filter((card) => chapters[chapterId].cardsRef.includes(card.id));
                return (
                  <>
                    {
                      <ChapterBox
                        key={chapterId}
                        index={index}
                        column={column}
                        cards={cardsRef}
                        onClick={onClick}
                        onDelete={onDelete}
                      />
                    }
                  </>
                );
              })}
            {provided.placeholder}
          </div>
        )}
      </Droppable>
    </DragDropContext>
  );
};

DraggableList.propTypes = {
  cards: PropTypes.object.isRequired,
  chapters: PropTypes.object.isRequired,
  chaptersOrder: PropTypes.array,
  changeOrder: PropTypes.func.isRequired,
};

export default DraggableList;
