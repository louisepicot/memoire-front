import React, { useState, useEffect, useRef } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import {
  changeOrder,
  saveChapter,
  deleteChapter,
} from "../../redux/actions/chapterActions";
import DraggableList from "./DraggableListTest";
import ChapterForm from "../functional-components/components/ChapterForm";
import SubChapterForm from "../functional-components/components/ChapterForm";

const newChapter = {
  id: null,
  title: "",
  cardsRef: [],
  slug: "",
};

const ChaptersContainer = ({
  cards,
  changeOrder,
  chapters,
  deleteChapter,
  saveChapter,
  chaptersOrder,
  history,
  ...props
}) => {
  const [addChapter, setAddChapter] = useState(false);
  const [addSubChapter, setAddSubChapter] = useState(false);
  const [chapter, setChapter] = useState({ ...props.chapter });
  const [errors, setErrors] = useState({});
  const [saving, setSaving] = useState(false);
  const btnTopChapter = useRef();

  useEffect(() => {
    setChapter(newChapter);
  }, [props.chapter, chapters]);

  const handleDeleteChapter = async (chapter) => {
    // toast.success("card deleted");

    // deleteChapter

    try {
      await deleteChapter(chapter);
      let newCardsRef = [...chapters.draft.cardsRef];
      chapter.cardsRef.map((cardRef) => {
        newCardsRef.push(cardRef);
      });
      let newChap = {
        ...chapters.draft,
        cardsRef: newCardsRef,
      };
      await saveChapter(newChap);
    } catch (error) {
      // toast.error("Delete failed. " + error.message, { autoClose: false });
      alert("Delete failed. " + error.message);
    }
  };

  const handleClickAddChapter = () => {
    setAddChapter(!addChapter);
  };
  const handleClickAddSubChapter = () => {
    setAddSubChapter(!addChapter);
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setChapter((prevChapter) => ({
      ...prevChapter,
      [name]: value,
    }));
  };

  const formIsValid = () => {
    const { title, items } = chapter;
    const errors = {};

    if (!title) errors.title = "Title is required.";
    // if (!items) errors.items = "content is required";

    setErrors(errors);
    // Form is valid if the errors object still has no properties
    return Object.keys(errors).length === 0;
  };

  const handleSave = (e) => {
    e.preventDefault();
    // if (!formIsValid()) return;
    // setSaving(true);
    saveChapter(chapter)
      .then(() => {
        setAddChapter(false);
        setSaving(false);
      })
      .catch((error) => {
        setSaving(false);
        setErrors({ onSave: error.message });
      });
  };
  const goTop = (e) => {
    btnTopChapter.current.scrollTo({
      top: 0,
      left: 0,
      behavior: "smooth",
    });
  };
  return (
    <div ref={btnTopChapter} className="chapter-container">
      {Object.keys(chapters)[0] && Object.keys(cards)[0] && (
        <>
          {addChapter && (
            <ChapterForm
              chapterSelected={chapter}
              onSave={handleSave}
              onChange={handleChange}
            ></ChapterForm>
          )}
          {addSubChapter && (
            <SubChapterForm
              chapterSelected={chapter}
              onSave={handleSave}
              onChange={handleChange}
            ></SubChapterForm>
          )}
          <DraggableList
            chaptersOrder={chaptersOrder}
            cards={cards}
            chapters={chapters}
            changeOrder={changeOrder}
            onDelete={handleDeleteChapter}
          ></DraggableList>

          <div className="btn-add">
            <button onClick={handleClickAddChapter}>
              {!addChapter ? "Add chapter ⊕" : "cancel"}
            </button>
            {/* <button onClick={handleClickAddSubChapter}>
              {!addSubChapter ? "Add subchapter ⊕" : "cancel"}
            </button> */}
          </div>
          <button className="btn-top" onClick={goTop}>
            ⇡Top⇡
          </button>
        </>
      )}
    </div>
  );
};

ChaptersContainer.propTypes = {
  chapter: PropTypes.object.isRequired,
  cards: PropTypes.object.isRequired,
  chapters: PropTypes.object.isRequired,
  changeOrder: PropTypes.func.isRequired,
  chapterOrder: PropTypes.array,
};

const mapStateToProps = (state) => {
  return {
    chapter: newChapter,
    cards: Object.keys(state.filter).length > 0 ? state.filter : state.cards,
    chapters: state.chapters,
    chaptersOrder: Object.keys(state.chapters).map(
      (key) => state.chapters[key].id
    ),
    loading: state.apiCallsInProgress > 0,
  };
};

const mapDispatchToProps = {
  changeOrder,
  saveChapter,
  deleteChapter,
};

export default connect(mapStateToProps, mapDispatchToProps)(ChaptersContainer);
