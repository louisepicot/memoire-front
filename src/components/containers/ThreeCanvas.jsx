import React, { useState, useRef, useEffect } from "react";
import { Canvas, useFrame, extend, useThree } from "react-three-fiber";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader";
import PropTypes from "prop-types";
import { useSpring, a } from "react-spring/three";

extend({ OrbitControls });

const Controls = () => {
  const orbitRef = useRef();
  const { camera, gl } = useThree();
  useFrame(() => orbitRef.current.update());
  return (
    <orbitControls autoRotate ref={orbitRef} args={[camera, gl.domElement]} />
  );
};

const Plane = () => {
  return (
    <mesh rotation={[-Math.PI / 2, 0, 0]} position={[0, -0.5, 0]}>
      <planeBufferGeometry attach="geometry" args={[100, 100]} />
      <meshPhysicalMaterial attach="material" color="red" />
    </mesh>
  );
};

const Ordi = () => {
  const [model, setModel] = useState();
  useEffect(() => {
    new GLTFLoader().load("/blender-obj/ordi-screen-blue-anim.glb", setModel);
  }, []);
  return model ? <primitive object={model.scene}></primitive> : null;
};

const Box = () => {
  const meshRef = useRef();
  const [Hovered, setHovered] = useState(false);
  const [Active, setActive] = useState(false);
  const props = useSpring({
    color: Hovered ? "grey" : "darkblue",
    scale: Active ? [1.5, 1.5, 1.5] : [1, 1, 1],
  });

  //   useFrame(() => (meshRef.current.rotation.y += 0.01));
  return (
    <a.mesh
      ref={meshRef}
      onPointerOver={() => setHovered(true)}
      onPointerOut={() => setHovered(false)}
      onClick={() => setActive(!Active)}
      scale={props.scale}
    >
      <boxBufferGeometry attach="geometry" args={[1, 1, 1]} />
      <a.meshPhysicalMaterial attach="material" color={props.color} />
    </a.mesh>
  );
};

const ThreeCanvas = () => {
  return (
    <Canvas className="canvas" camera={{ position: [0, 3, 4.5] }}>
      <ambientLight />
      {/* <fog attach="fog" args={["white", 10, 15]} /> */}
      <ambientLight intensity={1} />
      <pointLight position={[0, 1000, 50]} intensity={30} />
      <pointLight position={[50, -1000, 0]} intensity={30} />
      {/* <spotLight position={[-105, -35, 155]} intensity={0.5} />
      <spotLight position={[155, -100, 55]} intensity={0.5} />
      <spotLight position={[355, 500, 200]} intensity={0.5} /> */}
      <Controls />
      {/* <Plane /> */}
      {/* <Box /> */}
      <Ordi />
    </Canvas>
  );
};

ThreeCanvas.propTypes = {};

export default ThreeCanvas;
