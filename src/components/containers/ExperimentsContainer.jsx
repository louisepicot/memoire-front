import React, { useState, useEffect, useRef } from "react";
import PropTypes from "prop-types";

const ExperimentsContainer = () => {
  const btnTopChapter = useRef();

  const handleClickAddCard = () => {};

  const goTop = (e) => {
    btnTopChapter.current.scrollTo({
      top: 0,
      left: 0,
      behavior: "smooth",
    });
  };

  return (
    <div className="expe-container">
      {/* <iframe src="http://localhost:8084" frameBorder="0"></iframe>
      <iframe src="http://localhost:8082" frameBorder="0"></iframe>
      <iframe src="http://localhost:8085" frameBorder="0"></iframe>
      <iframe src="http://localhost:8081/louise/" frameBorder="0"></iframe>

      <iframe src="http://localhost:8083" frameBorder="0"></iframe> */}

      <iframe
        src="https://louisepicot.gitlab.io/talking-stick/"
        frameBorder="0"
      ></iframe>
      <iframe
        src="https://louisepicot.gitlab.io/opentype-js-test/"
        frameBorder="0"
      ></iframe>
      {/* <iframe
        src="https://louisepicot.gitlab.io/dirty-letters"
        frameBorder="0"
      ></iframe> */}

      <iframe
        src="https://louisepicot.gitlab.io/blue-falling-letters"
        frameBorder="0"
      ></iframe>
      {/* 
      <button className="btn-add" onClick={handleClickAddCard}>
        Add Links ⊕
      </button>
      <button className="btn-top" onClick={goTop}>
        ⇡Top⇡
      </button> */}
    </div>
  );
};

ExperimentsContainer.propTypes = {};

export default ExperimentsContainer;
