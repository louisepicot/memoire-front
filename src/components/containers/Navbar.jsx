import React, { useEffect, useState, useRef } from "react";
import { connect } from "react-redux";
import $ from "jquery";
import { filteredByTags } from "../../redux/actions/cardActions";
import TagsSearch from "../functional-components/components/TagsSearch";
import { Link } from "react-router-dom";

const NavBar = ({ cards, tags, filteredByTags }) => {
  // const [inputValue, setInputValue] = useState("");
  // const [adLinkArray, setPadLinkArray] = useState([]);

  // const onInputChange = (e) => {
  //   this.setState({ inputValue: e.target.value });
  // };

  // const onSubmit = (e) => {
  //   let { inputValue } = this.state;
  //   e.preventDefault();

  //   if (!inputValue) {
  //     return;
  //   }

  //   inputValue = inputValue.replace(/\s+/, "");
  //   const padLinkArray = inputValue.split("https://");
  //   this.setState({ padLinkArray });
  //   padLinkArray.forEach((padLink, i) => {
  //     const padName = padLink.replace(/\s+|^.+(\/p\/)/, "");
  //     if (padLink !== "") {
  //       const id = this.props.cards.length - 1;
  //       // this.props.fetchTexts(padLink, padName, id);
  //     }
  //   });
  // };

  useEffect(() => {}, [cards, tags]);

  const onClick = (e) => {
    document.querySelectorAll(".card").forEach((card) => {
      // card.style.columns = "2";
      card.style.maxHeight = "inherit";
      card.style.maxWidth = "inherit";
      // card.style.display = "inherit";
      card.style.overflow = "inherit";
      card.style.borderBottom = "inherit";
      card.style.fontSize = "14px";
      card.style.lineHeight = "18px";
      document.body.insertAdjacentElement("beforeend", card);
    });
    document.querySelectorAll("button").forEach((button) => {
      button.style.display = "none";
    });
    // document.querySelector(".big-grid").classList.remove("big-grid");
    // document.querySelector(".text-trigger").classList.add("hide");
    document.querySelector("nav").classList.add("hide");
    // document.querySelector("#reorder-list").classList.add("hide");
    document.querySelector(".cards-container").classList.add("hide");
    // document.querySelector(".intro-container").classList.add("hide");
    // document.querySelector(".grid-view").classList.add("hide");
    // document.querySelector(".big-grid").classList.add("hide");
    document.querySelector("#app").classList.add("hide");
    $.getScript(
      "https://unpkg.com/pagedjs/dist/paged.polyfill.js",
      function (data, textStatus, jqxhr) {}
    );
  };

  return (
    <nav id="nav">
      <button id="button-paged" onClick={onClick}>
        Click to print
      </button>
      {/* <Link to="cardsPrint" className="btn btn-primary btn-lg">
        outil pour mémoire
      </Link> */}
      <div className="tag-form">
        <h2>Recherche par tags</h2>
        {cards && (
          <TagsSearch
            cards={cards}
            tags={tags}
            filteredByTags={filteredByTags}
          ></TagsSearch>
        )}
      </div>
    </nav>
  );
};

const mapStateToProps = (state) => {
  let cards;
  if (Object.keys(state.filter).length > 0) {
    cards = state.filter;
  } else {
    cards = state.cards;
  }
  let cardsArr = Object.keys(cards).map((key) => cards[key]);
  let newTags = state.tags.map((tag) => tag.name);
  cardsArr.map((card) => {
    if (card.tags) {
      card.tags.map((tag) => (newTags.includes(tag) ? "" : newTags.push(tag)));
    }
  });

  console.log("tags", cardsArr, newTags, filteredByTags);
  return {
    cards: cardsArr,
    tags: newTags,
    navList: state.navList,
    orderList: state.orderList,
  };
};

const mapDispatchToProps = {
  filteredByTags,
};

export default connect(mapStateToProps, mapDispatchToProps)(NavBar);
