import React, { useEffect, useState, useRef } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Redirect } from "react-router-dom";
import * as showdown from "showdown";
import {
  loadCards,
  deleteCard,
  saveCard,
  filteredByTags,
} from "../../redux/actions/cardActions";
import { loadChapters, saveChapter } from "../../redux/actions/chapterActions";
import { loadTags } from "../../redux/actions/tagActions";
import CardList from "../functional-components/components/CardsList";
import Spinner from "../functional-components/common/Spinner";
import CardForm from "../functional-components/components/CardForm";
import TagsSearch from "../functional-components/components/TagsSearch";

const newCard = {
  id: null,
  title: "",
  tagsId: [],
  content: "",
  slug: "",
};

// import { toast } from "react-toastify";

const CardsContainer = ({
  ownProps,
  cards,
  chapters,
  chaptersNormal,
  tags,
  history,
  loadChapters,
  saveChapter,
  filteredByTags,
  loadCards,
  loadTags,
  deleteCard,
  saveCard,
  loading,
  filter,
  showFormInList,
  draft,
  ...props
}) => {
  // TODO REDIRECTION
  // const [redirectToAddCardPage, setRedirectToAddCardPage] = useState(false);
  const [card, setCard] = useState({ ...props.card });
  const [errors, setErrors] = useState({});
  const [saving, setSaving] = useState(false);
  const [addCard, setAddCard] = useState(false);
  const btnTop = useRef();

  useEffect(() => {
    if (cards.length === 0) {
      loadCards().catch((error) => {
        alert("Loading cards failed" + error);
      });
    } else {
      setCard({ ...props.card });
    }
    if (showFormInList) {
      setAddCard(false);
    }
    if (tags.length === 0) {
      loadTags().catch((error) => {
        alert("Loading tags failed" + error);
      });
    }
    if (chapters.length === 0) {
      loadChapters().catch((error) => {
        alert("Loading cards failed" + error);
      });
    }
  }, [props.card, showFormInList, filter]);

  const handleDeleteCard = async (e, card) => {
    e.target.parentNode.parentNode.style.marginLeft = "70vw";
    try {
      await deleteCard(card);
      let cardChapter;
      chaptersNormal.forEach((chapter, id) => {
        if (chapter.cardsRef.includes(card.id)) {
          cardChapter = chaptersNormal[id];
        }
      });
      let newCardsRef = [...cardChapter.cardsRef];
      let index = newCardsRef.indexOf(card.id);
      newCardsRef.splice(index, 1);
      await saveChapter({
        ...cardChapter,
        cardsRef: newCardsRef,
      });
      // await history.push("/cards");
    } catch (error) {
      // toast.error("Delete failed. " + error.message, { autoClose: false });
      alert("Delete failed. " + error.message);
    }
  };

  const convertCardMdToHtml = (card) => {
    let converter = new showdown.Converter();
    let content = converter.makeHtml(card.content);
    return content;
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setCard((prevCard) => ({
      ...prevCard,
      [name]: name === "tags" ? value.split(",") : value,
    }));
  };

  const formIsValid = () => {
    const { title, content } = card;
    const errors = {};

    if (!title) errors.title = "Title is required.";
    if (!content) errors.content = "content is required";

    setErrors(errors);
    // Form is valid if the errors object still has no properties
    return Object.keys(errors).length === 0;
  };

  const handleSave = (e) => {
    e.preventDefault();
    if (!formIsValid()) return;
    setSaving(true);
    saveCard(card)
      .then(() => {
        history.push("/cards");
        setAddCard(false);
        setSaving(false);
        filteredByTags({}, []);
      })
      .catch((error) => {
        setSaving(false);
        setErrors({ onSave: error.message });
      });
  };

  const handleClickAddCard = () => {
    setAddCard(!addCard);
    setCard(newCard);
    history.push("/cards");
    btnTop.current.scrollTo({
      top: 0,
      left: 0,
    });
  };

  const goTop = (e) => {
    btnTop.current.scrollTo({
      top: 0,
      left: 0,
      behavior: "smooth",
    });
  };

  return (
    <section ref={btnTop} className="cards-container">
      {/* <h2>cards</h2> */}
      {loading ? (
        <Spinner />
      ) : (
        <>
          {/* <div className="tag-form">
            <h2>Recherche par tags</h2>
            <TagsSearch
              cards={cards}
              tags={tags}
              filteredByTags={filteredByTags}
            ></TagsSearch>
          </div> */}
          {/* <button onClick={handleClickAddCard}>Add Card</button> */}
          {addCard && (
            <CardForm
              onCancel={handleClickAddCard}
              cardSelected={card}
              errors={errors}
              tags={tags}
              onChange={handleChange}
              onSave={handleSave}
              saving={saving}
            />
          )}
          {chapters[0] && (
            <CardList
              chapters={chapters}
              errors={errors}
              tags={tags}
              onChange={handleChange}
              onSave={handleSave}
              saving={saving}
              cardSelected={card}
              onDeleteClick={handleDeleteCard}
              convertCardMdToHtml={convertCardMdToHtml}
            />
          )}
        </>
      )}
      <button className="btn-add" onClick={handleClickAddCard}>
        Add Card ⊕
      </button>
      <button className="btn-top" onClick={goTop}>
        ⇡Top⇡
      </button>
    </section>
  );
};

CardsContainer.propTypes = {
  saveCard: PropTypes.func.isRequired,
  tags: PropTypes.array.isRequired,
  cards: PropTypes.array.isRequired,
  loadCards: PropTypes.func.isRequired,
  loadTags: PropTypes.func.isRequired,
  saveChapter: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
  deleteCard: PropTypes.func.isRequired,
  showFormInList: PropTypes.bool.isRequired,
  filteredByTags: PropTypes.func.isRequired,
  draft: PropTypes.object,
};

const selectCardFromSlug = (slug, cards) => {
  return cards.find((card) => card.id === slug) || null;
};

const mapStateToProps = (state, ownProps) => {
  let cards;
  if (Object.keys(state.filter).length > 0) {
    cards = state.filter;
  } else {
    cards = state.cards;
  }
  let cardsArr = Object.keys(cards).map((key) => cards[key]);

  const showFormInList = ownProps.propsFromRoute.match.path === "/card/:slug";
  const slug = ownProps.propsFromRoute.match.params.slug;
  const card =
    slug && cardsArr.length > 0 ? selectCardFromSlug(slug, cardsArr) : newCard;

  let chapters = Object.keys(state.chapters).map((key) => state.chapters[key]);
  chapters = chapters.map((chapter) => {
    let cardsRef = chapter.cardsRef.map((cardId) => cards[cardId]);
    return { ...chapter, cardsRef };
  });

  let chaptersNormal = Object.keys(state.chapters).map(
    (key) => state.chapters[key]
  );
  let newTags = state.tags.map((tag) => tag.name);
  cardsArr.map((card) => {
    if (card.tags) {
      card.tags.map((tag) => (newTags.includes(tag) ? "" : newTags.push(tag)));
    }
  });

  return {
    history: ownProps.propsFromRoute.history,
    showFormInList,
    card,
    cards: cardsArr,
    chapters,
    chaptersNormal,
    tags: newTags,
    loading: state.apiCallsInProgress > 0,
  };
};

const mapDispatchToProps = {
  loadCards,
  loadTags,
  loadChapters,
  saveChapter,
  deleteCard,
  saveCard,
  filteredByTags,
};

export default connect(mapStateToProps, mapDispatchToProps)(CardsContainer);
