import React from "react";
import { Route, Switch } from "react-router-dom";
import HomePage from "./functional-components/layouts/HomePage";
import PageNotFound from "./PageNotFound";
import FirstStepThesis from "./functional-components/layouts/FirstStepThesis";
import PrintPage from "./functional-components/layouts/PrintPage";
function App() {
  return (
    <div id="app">
      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route path="/cards" component={FirstStepThesis} />
        <Route path="/cardsPrint" component={PrintPage} />
        <Route path="/card/:slug" component={FirstStepThesis} />
        <Route component={PageNotFound} />
      </Switch>
    </div>
  );
}

export default App;
