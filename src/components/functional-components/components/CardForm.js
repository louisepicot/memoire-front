import React from "react";
import PropTypes from "prop-types";
import TextInput from "../common/TextInput";
import TextArea from "../common/TextArea";
import DataListInput from "../common/DataListInput";
import { Link } from "react-router-dom";

const CardForm = ({
  onDeleteClick,
  cardSelected,
  tags,
  onSave,
  onCancel,
  onChange,
  saving = false,
  errors = {},
}) => {
  return (
    <form onSubmit={onSave}>
      {/* <h2>{cardSelected.id ? "Edit" : "Add"} cardSelected</h2> */}
      {errors.onSave && (
        <div className="alert alert-danger" role="alert">
          {errors.onSave}
        </div>
      )}
      <div className={"card"}>
        <div className="top">
          <TextInput
            name="title"
            label="Title"
            value={cardSelected.title}
            onChange={onChange}
            error={errors.title}
          />
          <button type="submit" disabled={saving} className="">
            {saving ? "Saving..." : "Save"}
          </button>
          <Link to="/cards" onClick={onCancel} className="">
            <button className="">cancel</button>
          </Link>
        </div>
        <div className="content">
          <TextArea
            name="content"
            label="content"
            value={cardSelected.content}
            onChange={onChange}
            error={errors.content}
          />
        </div>
        <div className="bottom">
          {/* <SelectInput
            name="tags"
            label="tag"
            value={cardSelected.tagId || ""}
            defaultOption="Select tag"
            options={tags.map((tag) => ({
              value: tag.id,
              text: tag.name,
            }))}
            onChange={onChange}
            error={errors.tag}
          /> */}
          <DataListInput
            name="tags"
            label="tags"
            tagsList={tags}
            value={cardSelected.tags}
            onChange={onChange}
            error={errors.tags}
          />
        </div>
      </div>
    </form>
  );
};

CardForm.propTypes = {
  tags: PropTypes.array.isRequired,
  cardSelected: PropTypes.object.isRequired,
  errors: PropTypes.object,
  onSave: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  saving: PropTypes.bool,
};

export default CardForm;
