import React from "react";
import PropTypes from "prop-types";
import Card from "./Card";
import CardForm from "./CardForm";

export const CardsList = ({
  chapters,
  onDeleteClick,
  convertCardMdToHtml,
  cardSelected,
  errors,
  tags,
  onChange,
  onSave,
  saving,
}) => {
  return (
    <>
      {chapters.map((chapter) => (
        <div
          key={`list${chapter.id}`}
          className={chapter.id === "draft" ? "last-item" : ""}
        >
          {chapter.cardsRef[0] && <h1>{chapter.title}</h1>}
          {chapter.cardsRef.map((card) =>
            card && cardSelected.id === card.id ? (
              <CardForm
                key={`list${card.id}`}
                cardSelected={cardSelected}
                errors={errors}
                tags={tags}
                onDeleteClick={onDeleteClick}
                onChange={onChange}
                onSave={onSave}
                saving={saving}
              />
            ) : (
              <>
                {card && (
                  <Card
                    card={card}
                    key={`list${card.id}`}
                    onDeleteClick={onDeleteClick}
                    convertCardMdToHtml={convertCardMdToHtml}
                  />
                )}
              </>
            )
          )}
        </div>
      ))}
    </>
  );
};

CardsList.propTypes = {
  onDeleteClick: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  onSave: PropTypes.func.isRequired,
  convertCardMdToHtml: PropTypes.func.isRequired,
  searchTag: PropTypes.array,
};

export default CardsList;
