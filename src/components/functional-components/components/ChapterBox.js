import React from "react";
import PropTypes from "prop-types";
import { Droppable, Draggable } from "react-beautiful-dnd";

const ChapterBox = ({ cards, column, index, onClick, onDelete }) => {
  return (
    <Draggable draggableId={column.id} index={index} isDragDisabled={true}>
      {(provided) => (
        <div
          {...provided.draggableProps}
          ref={provided.innerRef}
          className={column.id === "draft" ? "last-item" : ""}
        >
          <div className="title-chapter">
            <h2 {...provided.dragHandleProps}>{column.title}</h2>
            {column.id !== "draft" && (
              <button
                className="btn btn-outline-danger"
                onClick={() => onDelete(column)}
              >
                <span aria-label="cross" role="img">
                  ×
                </span>
              </button>
            )}
          </div>
          <Droppable droppableId={column.id}>
            {(provided, snapshot) => (
              <ul
                ref={provided.innerRef}
                className={
                  snapshot.isDraggingOver ? "sub-list dropOver" : "sub-list"
                }
                {...provided.droppableProps}
              >
                {cards.map((card, index) => {
                  return (
                    <>
                      {card && (
                        <Draggable
                          key={card.id}
                          draggableId={card.id}
                          index={index}
                        >
                          {(provided, snapshot) => (
                            <li
                              className={
                                snapshot.isDragging ? "grabbing" : "white"
                              }
                              {...provided.draggableProps}
                              ref={provided.innerRef}
                              onClick={(e) => onClick(e, card)}
                            >
                              <p {...provided.dragHandleProps}>{card.title}</p>
                            </li>
                          )}
                        </Draggable>
                      )}
                    </>
                  );
                })}
                {provided.placeholder}
              </ul>
            )}
          </Droppable>
        </div>
      )}
    </Draggable>
  );
};

ChapterBox.propTypes = {
  onClick: PropTypes.func.isRequired,
  cards: PropTypes.array.isRequired,
  column: PropTypes.object.isRequired,
};

export default ChapterBox;
