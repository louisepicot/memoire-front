import React, { useEffect, useState, useRef } from "react";
import PropTypes from "prop-types";
import { WithContext as ReactTags } from "react-tag-input";

const KeyCodes = {
  comma: 188,
  enter: 13,
};

const delimiters = [KeyCodes.comma, KeyCodes.enter];

const TagsSearch = ({ tags, filteredByTags, cards, filter }) => {
  const [tagsState, setTagsState] = useState([]);
  const [suggestions, setSuggestions] = useState([]);

  useEffect(() => {
    if (tags.length > 0) {
      let sugg = tags.map((tag) => {
        return { id: tag, text: tag };
      });
      setSuggestions([...sugg]);
    }
  }, [tags]);

  const handleDelete = (i) => {
    setTagsState(tagsState.filter((tag, index) => index !== i));
    let newStateFilter = { ...cards };
    let arrTags = tagsState
      .filter((tag, index) => index !== i)
      .map((tagsObj) => tagsObj.text);

    if (arrTags[0]) {
      let arr = Object.keys(newStateFilter).map((key) => newStateFilter[key]);
      arr = arr.filter((card) => {
        let returnCard = false;
        if (card.tags) {
          returnCard = arrTags.every((el) => card.tags.includes(el));
        }
        return returnCard;
      });

      newStateFilter = {};
      arr.map((card) => {
        return (newStateFilter[card.id] = card);
      });
      if (Object.keys(newStateFilter).length === 0) {
        newStateFilter["empty"] = "empty";
      }
      // Object.keys(newStateFilter).length === 0 ?
    } else {
      newStateFilter = {};
    }
    filteredByTags(tagsState.filter((tag, index) => index !== i));
  };

  const handleAddition = (tag) => {
    setTagsState([...tagsState, tag]);
    // filtered([...tagsState, tag]);
    let newStateFilter = { ...cards };
    let arrTags = [...tagsState, tag].map((tagsObj) => tagsObj.text);

    if (arrTags[0]) {
      let arr = Object.keys(newStateFilter).map((key) => newStateFilter[key]);
      arr = arr.filter((card) => {
        let returnCard = false;
        if (card.tags) {
          returnCard = arrTags.every((el) => card.tags.includes(el));
        }
        return returnCard;
      });

      newStateFilter = {};
      arr.map((card) => {
        return (newStateFilter[card.id] = card);
      });
      if (Object.keys(newStateFilter).length === 0) {
        newStateFilter["empty"] = "empty";
      }
      // Object.keys(newStateFilter).length === 0 ?
    } else {
      newStateFilter = {};
    }
    filteredByTags(newStateFilter, arrTags);
  };

  const handleDrag = (tag, currPos, newPos) => {
    const newTags = [...tagsState].slice();
    newTags.splice(currPos, 1);
    newTags.splice(newPos, 0, tag);
    // re-render
    setTagsState(newTags);
    let newStateFilter = { ...cards };
    let arrTags = newTags.map((tagsObj) => tagsObj.text);

    if (arrTags[0]) {
      let arr = Object.keys(newStateFilter).map((key) => newStateFilter[key]);
      arr = arr.filter((card) => {
        let returnCard = false;
        if (card.tags) {
          returnCard = arrTags.every((el) => card.tags.includes(el));
        }
        return returnCard;
      });

      newStateFilter = {};
      arr.map((card) => {
        return (newStateFilter[card.id] = card);
      });
      if (Object.keys(newStateFilter).length === 0) {
        newStateFilter["empty"] = "empty";
      }
      // Object.keys(newStateFilter).length === 0 ?
    } else {
      newStateFilter = {};
    }
    filteredByTags(newStateFilter, arrTags);
    filteredByTags(newTags);
  };

  return (
    <div>
      {suggestions.length > 0 && (
        <ReactTags
          tags={tagsState}
          suggestions={suggestions}
          handleDelete={handleDelete}
          handleAddition={handleAddition}
          handleDrag={handleDrag}
          delimiters={delimiters}
        />
      )}
    </div>
  );
};

TagsSearch.propTypes = {
  filteredByTags: PropTypes.func.isRequired,
  tags: PropTypes.array.isRequired,
  cards: PropTypes.array.isRequired,
};

export default TagsSearch;
