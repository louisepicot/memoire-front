import React from "react";
import PropTypes from "prop-types";
import TextInput from "../common/TextInput";

const ChapterForm = ({
  chapterSelected,
  onSave,
  onChange,
  saving = false,
  errors = {},
}) => {
  return (
    <form onSubmit={onSave}>
      {/* <h2>{chapterSelected.id ? "Edit" : "Add"} chapterSelected</h2> */}
      {errors.onSave && (
        <div className="alert alert-danger" role="alert">
          {errors.onSave}
        </div>
      )}
      <div className="chapter-form">
        <TextInput
          name="title"
          label="Title"
          value={chapterSelected.title}
          onChange={onChange}
          error={errors.title}
        />
        <button type="submit" disabled={saving}>
          {saving ? "Saving..." : "Save"}
        </button>
      </div>
    </form>
  );
};

ChapterForm.propTypes = {
  chapterSelected: PropTypes.object.isRequired,
  errors: PropTypes.object,
  onSave: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  saving: PropTypes.bool,
};

export default ChapterForm;
