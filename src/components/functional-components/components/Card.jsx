import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

const Card = ({ card, onDeleteClick, convertCardMdToHtml }) => {
  return (
    <div className={"card"}>
      <div className="top">
        <h1>{card.title} </h1>
        <Link to={"/card/" + card.id}>
          {" "}
          <button className="btn btn-outline-danger">✍</button>
        </Link>
        <button
          className="btn btn-outline-danger"
          onClick={(e) => onDeleteClick(e, card)}
        >
          ⤫
        </button>
      </div>

      <div
        className="content"
        dangerouslySetInnerHTML={{ __html: convertCardMdToHtml(card) }}
      ></div>
      <div className="bottom">
        <ul>
          {card.tags &&
            card.tags.map((tag) => <li key={tag + card.id}>{tag}</li>)}
        </ul>
      </div>
    </div>
  );
};

Card.propTypes = {
  onDeleteClick: PropTypes.func.isRequired,
  convertCardMdToHtml: PropTypes.func.isRequired,
  card: PropTypes.object.isRequired,
};

export default Card;
