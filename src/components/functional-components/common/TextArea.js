import React from "react";
import PropTypes from "prop-types";

const TextArea = ({ name, label, onChange, placeholder, value, error }) => {
  return (
    <div className="input">
      <label htmlFor={name}></label>
      <textarea
        rows="40"
        autoComplete="on"
        type="text"
        name={name}
        placeholder={placeholder}
        value={value}
        onChange={onChange}
      />
      {error && <div>{error}</div>}
    </div>
  );
};

TextArea.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  placeholder: PropTypes.string,
  value: PropTypes.string,
  error: PropTypes.string,
};

export default TextArea;
