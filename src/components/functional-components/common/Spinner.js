import React from "react";
import "./Spinner.css";

const Spinner = () => {
  return (
    <div className="loader">
      <span role="img" aria-label="loader">
        👩🏻‍💻
      </span>
    </div>
  );
};

export default Spinner;
