import React from "react";
import PropTypes from "prop-types";

const DataListInput = ({
  name,
  label,
  tagsList,
  onChange,
  placeholder,
  value,
  error,
}) => {
  return (
    <div className="input">
      <label htmlFor={name}></label>
      <div>
        <input
          type="text"
          list="data"
          name={name}
          placeholder={placeholder}
          value={value}
          onChange={onChange}
        />
        <datalist id="data">
          {tagsList.map((item, key) => (
            <option key={key} value={item} />
          ))}
        </datalist>
        {error && <div>{error}</div>}
      </div>
    </div>
  );
};

DataListInput.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  placeholder: PropTypes.string,
  value: PropTypes.array,
  error: PropTypes.string,
  tagsList: PropTypes.array.isRequired,
};

export default DataListInput;
