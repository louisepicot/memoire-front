import React from "react";
import { Link } from "react-router-dom";
import ThreeCanvas from "../../containers/ThreeCanvas";

const HomePage = () => (
  <div>
    {/* <h1>salut</h1>
    <Link to="cards" className="btn btn-primary btn-lg">
      outil pour mémoire
    </Link> */}
    <ThreeCanvas></ThreeCanvas>
  </div>
);
export default HomePage;
