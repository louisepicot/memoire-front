import React from "react";
import { connect } from "react-redux";
import Navbar from "../../containers/Navbar";
import ThreeCanvas from "../../containers/ThreeCanvas";
import ChaptersContainer from "../../containers/ChaptersContainer";
import CardsContainer from "../../containers/CardsContainer";
import ExperimentsContainer from "../../containers/ExperimentsContainer";

const FirstStepThesis = ({ propsFromRoute }) => {
  return (
    <div id="app" className="big-grid">
      <Navbar />
      <ChaptersContainer />
      <CardsContainer propsFromRoute={propsFromRoute} />
      <ExperimentsContainer />
      {/* <ThreeCanvas></ThreeCanvas> */}
    </div>
  );
};

const mapStateToProps = (state, ownProps) => {
  return {
    propsFromRoute: ownProps,
  };
};

export default connect(mapStateToProps)(FirstStepThesis);
