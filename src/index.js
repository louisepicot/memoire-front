import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import initialState from "./redux/data";
import { BrowserRouter as Router } from "react-router-dom";
import { createBrowserHistory as history } from "history";
import * as serviceWorker from "./serviceWorker";
import "./assets/stylesheets/application.css";
import configureStore from "./redux/configureStore";
import App from "./components/App";
// import ManageCard from "./containers/ManageCard";

const store = configureStore(initialState);
// render an instance of the component in the DOM
ReactDOM.render(
  <Provider store={store}>
    <Router history={history}>
      <App />
    </Router>
  </Provider>,
  document.getElementById("root")
);
serviceWorker.unregister();
